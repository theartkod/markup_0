
/**
 * Created by vik_kod(vik_kod@mail.ru)
 */

'use strict';

const gulp              = require('gulp');
const browserSync       = require('browser-sync').create();
const rename            = require('gulp-rename');
const sass              = require('gulp-sass');
const pug               = require('gulp-pug');
const del               = require('del');
const plumber           = require('gulp-plumber');
const autoprefixer      = require('gulp-autoprefixer');
const babel             = require('gulp-babel');
const uglify            = require('gulp-uglify');


/********************************************************************/
/*HTML***************************************************************/
/********************************************************************/

gulp.task('html', () => {
    return gulp.src('./src/pug/*.pug')
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
});


/********************************************************************/
/*IMG****************************************************************/
/********************************************************************/

gulp.task('img', () => {
    return gulp.src('./src/images/*')
        .pipe(gulp.dest('./dist/images/'))
        .pipe(browserSync.stream());
});

/********************************************************************/
/*JSON***************************************************************/
/********************************************************************/

gulp.task('json', () => {
    return gulp.src('./src/json/*')
        .pipe(gulp.dest('./dist/json/'))
        .pipe(browserSync.stream());
});

/********************************************************************/
/*JS:DEV*************************************************************/
/********************************************************************/

gulp.task('js:dev', () => {
    return gulp.src('./src/js/*')
    // .pipe(sourcemaps.init())
        .pipe(babel({presets: ['es2015']}))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(plumber())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js/'))
        .pipe(browserSync.stream());
});

/********************************************************************/
/*FONTS**************************************************************/
/********************************************************************/

gulp.task('fonts', () => {
    return gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts/'))
        .pipe(browserSync.stream());
});


/********************************************************************/
/*CLEAN**************************************************************/
/********************************************************************/

gulp.task('clean', () => {
    return del(['./dist'], { read: false })
});

/********************************************************************/
/*STYLE:DEV**********************************************************/
/********************************************************************/

gulp.task('style', () => {
    return gulp.src('./src/style/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist/style/'))
        .pipe(browserSync.stream());
});

/********************************************************************/
/*WATCH**************************************************************/
/********************************************************************/

gulp.task('watch', () => {
    gulp.watch('./src/images/*',                gulp.series('img'));
gulp.watch('./src/**/*.pug',               gulp.series('html'));
gulp.watch('./src/style/**/*.scss',       gulp.series('style'));
gulp.watch('./src/style/**/*.scss',       gulp.series('style'));
gulp.watch('./src/js/**/*.*',            gulp.series('js:dev'));
gulp.watch('./src/json/**/*.*',            gulp.series('json'));
});

/********************************************************************/
/*SERVER*************************************************************/
/********************************************************************/

gulp.task('server', () => {
    return browserSync.init({
        server: {baseDir: './dist'},
        logPrefix: 'theartkod-markup',
        logFileChanges: false,
        reloadDelay: 1000,
        ghostMode: false,
        online: true
    });
});

/********************************************************************/
/*DEV****************************************************************/
/********************************************************************/

gulp.task('default',
    gulp.series('clean', 'html','style', 'img', 'fonts', 'js:dev', 'json',
        gulp.parallel('server', 'watch'))
);